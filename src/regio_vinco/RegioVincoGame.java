package regio_vinco;

import audio_manager.AudioManager;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.image.PixelReader;
import javafx.scene.image.PixelWriter;
import javafx.scene.image.WritableImage;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.Stage;
import pacg.PointAndClickGame;
import static regio_vinco.RegioVinco.*;

/**
 * This class is a concrete PointAndClickGame, as specified in The PACG
 * Framework. Note that this one plays Regio Vinco.
 *
 * @author McKillaGorilla
 * @coauthor Douglas Bell
 */
public class RegioVincoGame extends PointAndClickGame {

    // THIS PROVIDES GAME AND GUI EVENT RESPONSES
    RegioVincoController controller;

    // THIS PROVIDES MUSIC AND SOUND EFFECTS
    AudioManager audio;
    
    // THESE ARE THE GUI LAYERS
    Pane backgroundLayer;
    Pane gameLayer;
    Pane guiLayer;
    
    //variables for the stat display
    Label timer;
    Label regionsFound;
    Label regionsLeft;
    Label incorrectGuess;
    HBox stats;
    
    //variables for the victory stats display
    Label regionV;
    Label scoreV;
    Label timerV;
    Label subRegionsV;
    Label incorrectGuessV;
    VBox statsV;
    
    long timeStart;
    long timePassed;
    long finalTime;
    String finalTimeS;
    int finalScore = -1;
    
    /**
     * Get the game setup.
     */
    public RegioVincoGame(Stage initWindow) {
	super(initWindow, APP_TITLE, TARGET_FRAME_RATE);
	initAudio();
    }
    
    public AudioManager getAudio() {
	return audio;
    }
    
    public Pane getGameLayer() {
	return gameLayer;
    }

    /**
     * Initializes audio for the game.
     */
    private void initAudio() {
	audio = new AudioManager();
	try {
	    audio.loadAudio(TRACKED_SONG, TRACKED_FILE_NAME);
	    audio.play(TRACKED_SONG, true);

	    audio.loadAudio(AFGHAN_ANTHEM, AFGHAN_ANTHEM_FILE_NAME);
	    audio.loadAudio(SUCCESS, SUCCESS_FILE_NAME);
	    audio.loadAudio(FAILURE, FAILURE_FILE_NAME);
	} catch (Exception e) {
	    
	}
    }

    // OVERRIDDEN METHODS - REGIO VINCO IMPLEMENTATIONS
    // initData
    // initGUIControls
    // initGUIHandlers
    // reset
    // updateGUI
    /**
     * Initializes the complete data model for this application, forcing the
     * setting of all game data, including all needed SpriteType objects.
     */
    @Override
    public void initData() {
	// INIT OUR DATA MANAGER
	data = new RegioVincoDataModel();
	data.setGameDimensions(GAME_WIDTH, GAME_HEIGHT);

	boundaryLeft = 0;
	boundaryRight = GAME_WIDTH;
	boundaryTop = 0;
	boundaryBottom = GAME_HEIGHT;
    }

    /**
     * For initializing all GUI controls, specifically all the buttons and
     * decor. Note that this method must construct the canvas with its custom
     * renderer.
     */
    @Override
    public void initGUIControls() {
	// LOAD THE GUI IMAGES, WHICH INCLUDES THE BUTTONS
	// THESE WILL BE ON SCREEN AT ALL TIMES
	backgroundLayer = new Pane();
	addStackPaneLayer(backgroundLayer);
	addGUIImage(backgroundLayer, BACKGROUND_TYPE, loadImage(BACKGROUND_FILE_PATH), BACKGROUND_X, BACKGROUND_Y);
	
	
	// THEN THE GAME LAYER
	gameLayer = new Pane();
	addStackPaneLayer(gameLayer);
	
	// THEN THE GUI LAYER
	guiLayer = new Pane();
        guiLayer.getStylesheets().add(CSS_FILE);
	addStackPaneLayer(guiLayer);
	addGUIImage(guiLayer, TITLE_TYPE, loadImage(TITLE_FILE_PATH), TITLE_X, TITLE_Y);
        addGUIImage(guiLayer, R_T_TYPE, loadImage(REGION_TITLE_FILE_PATH), R_T_X, R_T_Y);
        guiImages.get(R_T_TYPE).setVisible(false);
	addGUIButton(guiLayer, START_TYPE, loadImage(START_BUTTON_FILE_PATH), START_X, START_Y);
	addGUIButton(guiLayer, EXIT_TYPE, loadImage(EXIT_BUTTON_FILE_PATH), EXIT_X, EXIT_Y);
        
        
	// NOTE THAT THE MAP IS ALSO AN IMAGE, BUT
	// WE'LL LOAD THAT WHEN A GAME STARTS, SINCE
	// WE'LL BE CHANGING THE PIXELS EACH TIME
	// FOR NOW WE'LL JUST LOAD THE ImageView
	// THAT WILL STORE THAT IMAGE
	ImageView mapView = new ImageView();
	mapView.setX(MAP_X);
	mapView.setY(MAP_Y);
	guiImages.put(MAP_TYPE, mapView);
	guiLayer.getChildren().add(mapView);

       
        //STATS
        timer = new Label();
        timer.setFont(Font.font("Times New Roman", FontWeight.BOLD, 23));
        timer.setStyle("-fx-text-fill: darkgoldenrod");
        regionsFound = new Label();
        regionsFound.setFont(Font.font("Times New Roman", FontWeight.BOLD, 23));
        regionsFound.setStyle("-fx-text-fill: darkgoldenrod");
        regionsLeft = new Label();
        regionsLeft.setFont(Font.font("Times New Roman", FontWeight.BOLD, 23));
        regionsLeft.setStyle("-fx-text-fill: darkgoldenrod");
        incorrectGuess = new Label();
        incorrectGuess.setFont(Font.font("Times New Roman", FontWeight.BOLD, 23));
        incorrectGuess.setStyle("-fx-text-fill: darkgoldenrod");
        stats = new HBox(20, timer, regionsFound, regionsLeft, incorrectGuess);
        stats.relocate(62, 640);
        stats.setVisible(false);
        guiLayer.getChildren().add(stats);
        
         
	// NOW LOAD THE WIN DISPLAY, WHICH WE'LL ONLY
	// MAKE VISIBLE AND ENABLED AS NEEDED
	ImageView winView = addGUIImage(guiLayer, WIN_DISPLAY_TYPE, loadImage(WIN_DISPLAY_FILE_PATH), WIN_X, WIN_Y);
	winView.setVisible(false);
        
        //VICTORY STATS
        regionV = new Label("Region: Afghanistan");
        regionV.setFont(Font.font("Times New Roman", FontWeight.BOLD, 22));
        regionV.setStyle("-fx-text-fill: darkblue");
        scoreV = new Label();
        scoreV.setFont(Font.font("Times New Roman", FontWeight.BOLD, 22));
        scoreV.setStyle("-fx-text-fill: darkblue");
        timerV = new Label();
        timerV.setFont(Font.font("Times New Roman", FontWeight.BOLD, 22));
        timerV.setStyle("-fx-text-fill: darkblue");
        subRegionsV = new Label("Sub Regions: 34");
        subRegionsV.setFont(Font.font("Times New Roman", FontWeight.BOLD, 22));
        subRegionsV.setStyle("-fx-text-fill: darkblue");
        incorrectGuessV = new Label();
        incorrectGuessV.setFont(Font.font("Times New Roman", FontWeight.BOLD, 22));
        incorrectGuessV.setStyle("-fx-text-fill: darkblue");
        statsV = new VBox(17, regionV, scoreV, timerV, subRegionsV, incorrectGuessV);
        statsV.relocate(385, 307);
        statsV.setVisible(false);
        guiLayer.getChildren().add(statsV);
             
    }
    
    // HELPER METHOD FOR LOADING IMAGES
    private Image loadImage(String imagePath) {	
	Image img = new Image("file:" + imagePath);
	return img;
    }

    /**
     * For initializing all the button handlers for the GUI.
     */
    @Override
    public void initGUIHandlers() {
	controller = new RegioVincoController(this);

	Button startButton = guiButtons.get(START_TYPE);
	startButton.setOnAction(e -> {
            timeStart = System.currentTimeMillis();
	    controller.processStartGameRequest();
	});

	Button exitButton = guiButtons.get(EXIT_TYPE);
	exitButton.setOnAction(e -> {
	    controller.processExitGameRequest();
	});

	// MAKE THE CONTROLLER THE HOOK FOR KEY PRESSES
	keyController.setHook(controller);

	// SETUP MOUSE PRESSES ON THE MAP
	ImageView mapView = guiImages.get(MAP_TYPE);
	mapView.setOnMousePressed(e -> {
	    controller.processMapClickRequest((int) e.getX(), (int) e.getY());
	});
	
	// KILL THE APP IF THE USER CLOSES THE WINDOW
	window.setOnCloseRequest(e->{
	    controller.processExitGameRequest();
	});
    }

    /**
     * Called when a game is restarted from the beginning, it resets all game
     * data and GUI controls so that the game may start anew.
     */
    @Override
    public void reset() {
	// IF THE WIN DIALOG IS VISIBLE, MAKE IT INVISIBLE
	ImageView winView = guiImages.get(WIN_DISPLAY_TYPE);
	winView.setVisible(false);
        stats.setVisible(true);
        statsV.setVisible(false);
        
        guiImages.get(R_T_TYPE).setVisible(true);
        ImageView mapView = guiImages.get(MAP_TYPE);
        mapView.setVisible(true);

	// AND RESET ALL GAME DATA
	data.reset(this);
        finalTimeS = null;
        finalScore = -1;
    }

    /**
     * This mutator method changes the color of the debug text.
     *
     * @param initColor Color to use for rendering debug text.
     */
    public static void setDebugTextColor(Color initColor) {
//        debugTextColor = initColor;
    }

    /**
     * Called each frame, this method updates the rendering state of all
     * relevant GUI controls, like displaying win and loss states and whether
     * certain buttons should be enabled or disabled.
     */
    int backgroundChangeCounter = 0;

    @Override
    public void updateGUI() {
	// IF THE GAME IS OVER, DISPLAY THE APPROPRIATE RESPONSE
	if (data.won()) {
            
            //CALCULATE THE GAME DURATION IN SECONDS
            finalTime = (System.currentTimeMillis() - timeStart)/1000;
            
            if (finalTimeS == null)
                finalTimeS = ((RegioVincoDataModel)data).getSecondsAsTimeText(finalTime);
            
            if (finalScore == -1)
                finalScore = (int)((1000-finalTime) - (100 * incorrectGuesses));
            
            if (finalScore < 0)
                finalScore = 0;
            
	    ImageView winImage = guiImages.get(WIN_DISPLAY_TYPE);
            ImageView mapView = guiImages.get(MAP_TYPE);
            mapView.setVisible(false);
	    winImage.setVisible(true); 
            
            stats.setVisible(false);
            statsV.setVisible(true); 
            
            scoreV.setText("Score: " + finalScore);
            timerV.setText("Game Duration: " + finalTimeS);
            incorrectGuessV.setText("Incorrect Guesses: " + incorrectGuesses);
            
            data.pause();
                 
	}
        //update the stats on the bottom every time this gets called!
        timePassed = (System.currentTimeMillis() - timeStart)/1000;
        timer.setText("Elapsed Time: " + ((RegioVincoDataModel)data).getSecondsAsTimeText(timePassed));
        regionsFound.setText("Regions Found: " + ((RegioVincoDataModel)data).getRegionsFound());
        regionsLeft.setText("Regions Left: " + ((RegioVincoDataModel)data).getRegionsNotFound());
        incorrectGuess.setText("Incorrect Guesses: " + incorrectGuesses);
    }

    public void reloadMap() {
	Image tempMapImage = loadImage(AFG_MAP_FILE_PATH);
	PixelReader pixelReader = tempMapImage.getPixelReader();
	WritableImage mapImage = new WritableImage(pixelReader, (int) tempMapImage.getWidth(), (int) tempMapImage.getHeight());
	ImageView mapView = guiImages.get(MAP_TYPE);
	mapView.setImage(mapImage);
	int numSubRegions = ((RegioVincoDataModel) data).getRegionsFound() + ((RegioVincoDataModel) data).getRegionsNotFound();
	this.boundaryTop = -(numSubRegions * 50);

	// AND GIVE THE WRITABLE MAP TO THE DATA MODEL
	((RegioVincoDataModel) data).setMapImage(mapImage);
        
        //REMOVE THE ORANGE
        removeOrange(mapImage);
    }
    
    /**
     * Method to remove the outer orange parts of the map.  If the color matches
     * the pixel at (0,0) it'll be replaced.
     * @param mapImage
      
     */
    public void removeOrange(WritableImage mapImage){
        
        PixelReader pixelReader = mapImage.getPixelReader();
        PixelWriter pixelWriter = mapImage.getPixelWriter();
        
        Color color = pixelReader.getColor(0,0);
        
        for (int x = 0; x < mapImage.getWidth(); x++)
        {
            for (int y = 0; y < mapImage.getHeight(); y++)
            {
                if (pixelReader.getColor(x, y).equals(color))
                    {
                        pixelWriter.setColor(x, y, Color.BLACK);
                    }   
            }
        }
    }
}
